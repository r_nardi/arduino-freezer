//Libraries
#include <Adafruit_Sensor.h>;
#include <DHT_U.h>
#include <DHT.h>

//Constants
#define dhtPin (2));
#define DHTTYPE DHT22;
DHT dht(dhtPin, DHTTYPE); //// Inicializa o sensor para o arduino 16mhz

//Variables
int chk;
float umid;
float temp;

int bomba = 13;
int coolers = 12;
int vs1 = 11;
int vs2 = 10;

void setup() {
  Serial.begin(9600);
  dht.begin();
  pinMode(bomba, OUTPUT);
  pinMode(coolers, OUTPUT);
  pinMode(vs1, OUTPUT);
  pinMode(vs2, OUTPUT);
  
}

void loop() {
  umid=dht.readHumidity(); //lê umidade
  temp=dht.readTemperature(); //lê temperatura
  Serial.print("Umidade: ");
  Serial.print(umid);
  Serial.print(" %, Temperatura: ");
  Serial.print(temp);
  Serial.println(" Celsius");
  delay(2000); //Delay 2 sec.

  if (temp>18 and umid<80) {
    digitalWrite(bomba, HIGH); //liga bomba
    digitalWrite(coolers, HIGH); //liga coolers + pastilhas peltier
    digitalWrite(vs1, HIGH); //abre valvula do nebulizador
    digitalWrite(vs2, HIGH); //abre valvula dos coolers
    delay(10000); //permanece 10s nebulizando e arrefecendo
  }

  if (temp>18 and (umid>80 or umid=80)) {
    digitalWrite(bomba, HIGH); //liga bomba
    digitalWrite(coolers, HIGH); //liga coolers + pastilhas peltier
    digitalWrite(vs1, LOW); //abre valvula do nebulizador
    digitalWrite(vs2, HIGH); //abre valvula dos coolers
    delay(300000); //permanece 5 MINUTOS apenas arrefecendo
  }

  if ((temp<18 or temp=18) and umid<80) {
    digitalWrite(bomba, HIGH); //liga bomba
    digitalWrite(coolers, LOW); //liga coolers + pastilhas peltier
    digitalWrite(vs1, HIGH); //abre valvula do nebulizador
    digitalWrite(vs2, LOW); //abre valvula dos coolers
    delay(10000); //permanece 10s nebulizando e arrefecendo
  }

  IF ((temp<18 or temp=18) and (umid>80 or umid=80) {
    delay(2000);
  }


  
  }

}
