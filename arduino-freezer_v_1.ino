//Biblioteca

#include <LiquidCrystal.h>
#include <dht.h>
dht DHT;

//Constants
#define DHTPIN 7 //pino 7 do arduino ligado ao pino de dados do sensor

LiquidCrystal lcd(12, 11, 5, 4, 3, 2); // Pinos do display

#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE); // Definicoes do sensor : pino, tipo

// Array simbolo grau
byte grau[8] ={ B00001100, 
                B00010010, 
                B00010010, 
                B00001100, 
                B00000000, 
                B00000000, 
                B00000000, 
                B00000000,}; // Array simbolo grau

                
//Variables
int chk;
float umid;
float temp;

int bomba = 13;
int coolers = 12;
int vs1 = 11;
int vs2 = 10;

void setup() {

   // Inicializa o display:
  lcd.begin(16, 2);
  lcd.clear();
  // Cria o caracter customizado com o simbolo do grau
  lcd.createChar(0, grau); 
  // Informacoes iniciais no display
  lcd.setCursor(0,0);
  lcd.print("Temp. : ");
  lcd.setCursor(13,0);
  // Mostra o simbolo do grau
  lcd.write(byte(0));
  lcd.print("C");
  lcd.setCursor(0,1);
  lcd.print("Umid. : ");
  lcd.setCursor(14,1);
  lcd.print("%");
  
  
  Serial.begin(9600);
  dht.begin();
  pinMode(bomba, OUTPUT);
  pinMode(coolers, OUTPUT);
  pinMode(vs1, OUTPUT);
  pinMode(vs2, OUTPUT);
  
}

void loop() {
  delay(10);
  umid = dht.readHumidity(); //lê umidade
  temp = dht.readTemperature(); //lê temperatura
  
  // Mostra a temperatura no serial monitor e no display
  Serial.print("Temperatura: "); 
  Serial.print(temp);
  lcd.setCursor(8,0);
  lcd.print(temp);
  Serial.print(" *C  ");
  // Mostra a umidade no serial monitor e no display
  Serial.print("Umidade : "); 
  Serial.print(umid);
  Serial.println(" %");
  lcd.setCursor(8,1);
  lcd.print(umid);
  delay(2000); //delay 2 sec

  if (umid<80) {
    
    digitalWrite(vs1, HIGH);  //abre valvula do nebulizador
    digitalWrite(vs2, LOW); //fecha valvula dos coolers
    digitalWrite(bomba, HIGH); //liga bomba
    digitalWrite(coolers, LOW); //desliga coolers + pastilhas peltier
    delay(2000);
  }
  
  if (temp>18) {
    digitalWrite(vs1, LOW); //fecha valvula do nebulizador
    digitalWrite(vs2, HIGH); //abre valvula dos coolers
    digitalWrite(bomba, HIGH); //liga bomba
    digitalWrite(coolers, HIGH); //liga coolers + pastilhas peltier
    delay(600000); //permanece 10 MINUTOS apenas arrefecendo
  }

  delay(60000); //espera 1 minuto


  
  }

}
